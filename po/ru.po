# Russian translation for Endeavour.
# Copyright (C) 2022 Endeavour's COPYRIGHT HOLDER
# This file is distributed under the same license as the Endeavour package.
#
# Stas Solovey <whats_up@tut.by>, 2015.
# Yuri Myasoedov <ymyasoedov@yandex.ru>, 2017.
# Ivan Komaritsyn <vantu5z@mail.ru>, 2015, 2017.
# Дронова Ю <juliette.tux@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Endeavour main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/Endeavour/-/issues\n"
"POT-Creation-Date: 2022-09-02 16:36+0000\n"
"PO-Revision-Date: 2022-09-05 23:09+0300\n"
"Last-Translator: Aleksandr Melman <Alexmelman88@gmail.com>\n"
"Language-Team: Russian <gnome-cyr@gnome.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Poedit 3.1\n"

#: data/appdata/org.gnome.Todo.appdata.xml.in.in:6
#: data/org.gnome.Todo.desktop.in.in:3 src/gui/gtd-application.c:121
#: src/gui/gtd-omni-area.ui:44 src/main.c:37
msgid "Endeavour"
msgstr "Endeavour"

#: data/appdata/org.gnome.Todo.appdata.xml.in.in:7
msgid "Manage your tasks"
msgstr "Управляйте своими задачами"

#: data/appdata/org.gnome.Todo.appdata.xml.in.in:10
msgid ""
"Endeavour is a task management application designed for simplicity. Save and "
"order your todos. Manage multiple todo lists. And more"
msgstr ""
"Endeavour — приложение для управления задачами, разработанное для простоты в "
"использовании. Сохраняйте и сортируйте свои задачи. Управляйте несколькими "
"списками задач. И многое другое"

#: data/appdata/org.gnome.Todo.appdata.xml.in.in:23
msgid "Empty state"
msgstr "Пустое состояние"

#: data/appdata/org.gnome.Todo.appdata.xml.in.in:27
msgid "Task lists"
msgstr "Списки задач"

#: data/appdata/org.gnome.Todo.appdata.xml.in.in:131
msgid "Jamie Murphy"
msgstr "Jamie Murphy"

#: data/org.gnome.Todo.desktop.in.in:4
msgid "Manage your personal tasks"
msgstr "Управляйте своими персональными задачами"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Todo.desktop.in.in:13
msgid "Task;Productivity;Todo;"
msgstr "Задачи;Продуктивность;Todo;"

#: data/org.gnome.todo.gschema.xml:6
msgid "Window maximized"
msgstr "Окно развёрнуто"

#: data/org.gnome.todo.gschema.xml:7
msgid "Window maximized state"
msgstr "Окно имеет максимальный размер"

#: data/org.gnome.todo.gschema.xml:11
msgid "Window size"
msgstr "Размер окна"

#: data/org.gnome.todo.gschema.xml:12
msgid "Window size (width and height)."
msgstr "Размер окна (ширина и высота)."

#: data/org.gnome.todo.gschema.xml:16
msgid "First run of Endeavour"
msgstr "Первый запуск Endeavour"

#: data/org.gnome.todo.gschema.xml:17
msgid ""
"Whether it’s the first run of Endeavour (to run the initial setup) or not"
msgstr ""
"Является ли это первым запуском Endeavour (запускать начальную настройку) "
"или нет"

#: data/org.gnome.todo.gschema.xml:21
msgid "Default provider to add new lists to"
msgstr "Служба по умолчанию, в которую добавляются списки"

#: data/org.gnome.todo.gschema.xml:22
msgid "The identifier of the default provider to add new lists to"
msgstr "Идентификатор службы по умолчанию, в которую добавляются новые списки"

#: data/org.gnome.todo.gschema.xml:26
msgid "List of active extensions"
msgstr "Список активных расширений"

#: data/org.gnome.todo.gschema.xml:27
msgid "The list of active extensions"
msgstr "Список активных расширений"

#: data/org.gnome.todo.gschema.xml:31
msgid "Sidebar revealed"
msgstr "Показывать боковую панель"

#: data/org.gnome.todo.gschema.xml:32
msgid "Whether the sidebar is revealed or not"
msgstr "Определяет отображается боковая панель или нет"

#: src/gui/gtd-application.c:70
msgid "Quit Endeavour"
msgstr "Закрыть Endeavour"

#: src/gui/gtd-application.c:71
msgid "Enable debug messages"
msgstr "Включить отладочные сообщения"

#: src/gui/gtd-application.c:72
msgid "Print version information and exit"
msgstr "Вывести информацию о версии и выйти"

#: src/gui/gtd-application.c:124
msgid "Copyright © 2015–2022 The Endeavour authors"
msgstr "Авторские права © 2015–2022 Авторы приложения Endeavour"

#: src/gui/gtd-application.c:130
msgid "translator-credits"
msgstr ""
"Stas Solovey <whats_up@tut.by>, 2015\n"
"Ivan Komaritsyn <vantu5z@mail.ru>, 2015\n"
"Yuri Myasoedov <ymyasoedov@yandex.ru>, 2017\n"
"Alexey Rubtsov <rushills@gmail.com>, 2021\n"
"Aleksandr Melman <alexmelman88@gmail.com>, 2022"

#: src/gui/gtd-edit-pane.c:93
#: src/plugins/all-tasks-panel/gtd-all-tasks-panel.c:111
#: src/plugins/next-week-panel/gtd-next-week-panel.c:140
#: src/plugins/scheduled-panel/gtd-panel-scheduled.c:103
msgid "No date set"
msgstr "Дата не установлена"

#: src/gui/gtd-edit-pane.ui:16
msgid "D_ue Date"
msgstr "_Дата выполнения"

#: src/gui/gtd-edit-pane.ui:30
msgid "_Today"
msgstr "_Сегодня"

#: src/gui/gtd-edit-pane.ui:39
msgid "To_morrow"
msgstr "_Завтра"

#: src/gui/gtd-edit-pane.ui:75
msgid "_Notes"
msgstr "_Заметки"

#: src/gui/gtd-edit-pane.ui:121
msgid "_Delete"
msgstr "_Удалить"

#: src/gui/gtd-edit-pane.ui:158
msgctxt "taskdate"
msgid "None"
msgstr "Нет"

#: src/gui/gtd-initial-setup-window.ui:20
msgid "Welcome"
msgstr "Добро пожаловать"

#: src/gui/gtd-initial-setup-window.ui:31
msgid "Log in to online accounts to access your tasks"
msgstr "Войдите в сетевые учётные записи, чтобы получить доступ к задачам"

#: src/gui/gtd-initial-setup-window.ui:53
msgid "Endeavour Setup"
msgstr "Настройка Endeavour"

#: src/gui/gtd-initial-setup-window.ui:56 src/gui/gtd-provider-popover.ui:42
msgid "_Cancel"
msgstr "_Отменить"

#: src/gui/gtd-initial-setup-window.ui:65
msgid "_Done"
msgstr "_Готово"

#. Translators: %1$s is the task list name, %2$s is the provider name
#: src/gui/gtd-new-task-row.c:90
#, c-format
msgid "%1$s \t <small>%2$s</small>"
msgstr "%1$s \t <small>%2$s</small>"

#: src/gui/gtd-new-task-row.c:127
msgid "An error occurred while creating a task"
msgstr "Ошибка при создании задачи"

#: src/gui/gtd-new-task-row.ui:14
msgid "New task…"
msgstr "Создать задачу…"

#: src/gui/gtd-provider-popover.c:97
msgid "An error occurred while creating a task list"
msgstr "Ошибка при создании списка задач"

#: src/gui/gtd-provider-popover.ui:25
msgid "Create _List"
msgstr "_Создать список"

#: src/gui/gtd-provider-popover.ui:55
msgid "List Name"
msgstr "Имя списка"

#: src/gui/gtd-provider-popover.ui:116
msgid "Select a storage location"
msgstr "Выберите место хранения"

#: src/gui/gtd-provider-row.ui:60
msgid "Off"
msgstr "Выключено"

#: src/gui/gtd-provider-selector.ui:22
msgid "Click to add a new Google account"
msgstr "Нажмите для добавления новой учётной записи Google"

#: src/gui/gtd-provider-selector.ui:38
msgid "Google"
msgstr "Google"

#: src/gui/gtd-provider-selector.ui:48
msgid "Click to add a new ownCloud account"
msgstr "Нажмите для добавления новой учётной записи ownCloud"

#: src/gui/gtd-provider-selector.ui:64
msgid "ownCloud"
msgstr "ownCloud"

#: src/gui/gtd-provider-selector.ui:74
msgid "Click to add a new Microsoft Exchange account"
msgstr "Нажмите для добавления новой учётной записи Microsoft Exchange"

#: src/gui/gtd-provider-selector.ui:90
msgid "Microsoft Exchange"
msgstr "Microsoft Exchange"

#: src/gui/gtd-provider-selector.ui:103
msgid "Or you can just store your tasks on this computer"
msgstr "Также можно хранить задания на компьютере"

#: src/gui/gtd-task-list-view.c:436
#, c-format
msgid "Task <b>%s</b> removed"
msgstr "Задача <b>%s</b> удалена"

#: src/gui/gtd-task-list-view.c:454
#: src/plugins/task-lists-workspace/gtd-sidebar.c:422
msgid "Undo"
msgstr "Отменить"

#: src/gui/gtd-task-row.c:127
#: src/plugins/all-tasks-panel/gtd-all-tasks-panel.c:132
#: src/plugins/next-week-panel/gtd-next-week-panel.c:152
#: src/plugins/scheduled-panel/gtd-panel-scheduled.c:124
#: src/plugins/today-panel/gtd-panel-today.c:177
#: src/plugins/today-panel/gtd-panel-today.c:295
msgid "Today"
msgstr "Сегодня"

#: src/gui/gtd-task-row.c:131
#: src/plugins/all-tasks-panel/gtd-all-tasks-panel.c:136
#: src/plugins/next-week-panel/gtd-next-week-panel.c:156
#: src/plugins/scheduled-panel/gtd-panel-scheduled.c:128
msgid "Tomorrow"
msgstr "Завтра"

#: src/gui/gtd-task-row.c:135
#: src/plugins/all-tasks-panel/gtd-all-tasks-panel.c:128
#: src/plugins/scheduled-panel/gtd-panel-scheduled.c:120
msgid "Yesterday"
msgstr "Вчера"

#: src/gui/gtd-window.c:87
msgid ""
"This is a development build of Endeavour. You may experience errors, wrong "
"behaviors, and data loss."
msgstr ""
"Это разрабатываемая версия Endeavour. Могут встречаться ошибки, нелогичное "
"поведение, а также потеря данных."

#: src/gui/menus.ui:7
msgid "_Help"
msgstr "_Справка"

#: src/gui/menus.ui:11
msgid "Keyboard Shortcuts"
msgstr "Комбинации Клавиш"

#: src/gui/menus.ui:15
msgid "_About Endeavour"
msgstr "_О Endeavour"

#: src/gui/shortcuts-dialog.ui:16
msgctxt "shortcut window"
msgid "General"
msgstr "Общие"

#: src/gui/shortcuts-dialog.ui:21
msgctxt "shortcut window"
msgid "Quit"
msgstr "Завершить"

#: src/gui/shortcuts-dialog.ui:28
msgctxt "shortcut window"
msgid "Help"
msgstr "Справка"

#: src/gui/shortcuts-dialog.ui:37
msgctxt "shortcut window"
msgid "Move to the panel/view up"
msgstr "Переместиться на панель/вид выше"

#: src/gui/shortcuts-dialog.ui:45
msgctxt "shortcut window"
msgid "Move to the panel/view below"
msgstr "Переместиться на панель/вид ниже"

#. Translators: This message will never be used with '1 day ago'
#. * but the singular form is required because some languages do not
#. * have plurals, some languages reuse the singular form for numbers
#. * like 21, 31, 41, etc.
#.
#: src/plugins/all-tasks-panel/gtd-all-tasks-panel.c:124
#: src/plugins/scheduled-panel/gtd-panel-scheduled.c:116
#, c-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "%d день назад"
msgstr[1] "%d дня назад"
msgstr[2] "%d дней назад"

#: src/plugins/all-tasks-panel/gtd-all-tasks-panel.c:322
msgid "All"
msgstr "Все"

#: src/plugins/eds/gtd-plugin-eds.c:181
msgid "Error loading GNOME Online Accounts"
msgstr "Ошибка загрузки сетевых учётных записей GNOME"

#: src/plugins/eds/gtd-provider-eds.c:190
msgid "Failed to connect to task list"
msgstr "Ошибка подключения к списку задач"

#: src/plugins/eds/gtd-provider-local.c:50
msgid "On This Computer"
msgstr "На этом компьютере"

#: src/plugins/eds/gtd-provider-local.c:62
msgid "Local"
msgstr "Локальные"

#: src/plugins/eds/gtd-task-list-eds.c:407
#: src/plugins/eds/gtd-task-list-eds.c:434
#: src/plugins/eds/gtd-task-list-eds.c:458
msgid "Error fetching tasks from list"
msgstr "Ошибка получения задач из списка"

#: src/plugins/eds/gtd-task-list-eds.c:828
#: src/plugins/inbox-panel/gtd-inbox-panel.c:111
msgid "Inbox"
msgstr "Входящие"

#: src/plugins/next-week-panel/gtd-next-week-panel.c:148
#: src/plugins/today-panel/gtd-panel-today.c:168
msgid "Overdue"
msgstr "Просроченные"

#: src/plugins/next-week-panel/gtd-next-week-panel.c:388
msgid "Next 7 Days"
msgstr "Следующие 7 дней"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:56
msgid "Did you drink some water today?"
msgstr "Вы сегодня пили хотя бы немного воды?"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:57
msgid "What are your goals for today?"
msgstr "Какие планы на сегодня?"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:58
msgid "Can you let your creativity flow?"
msgstr "Может добавить немного креатива?"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:59
msgid "How are you feeling right now?"
msgstr "Как Вы себя чувствуете?"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:60
msgid "At what point is it good enough?"
msgstr "Когда это будет достаточно хорошо?"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:65
msgid "Remember to breathe. Good. Don't stop."
msgstr "Не забывайте дышать. Вот так, хорошо. Не останавливайтесь."

#: src/plugins/peace/gtd-peace-omni-area-addin.c:66
msgid "Don't forget to drink some water"
msgstr "Не забывайте пить воду"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:67
msgid "Remember to take some time off"
msgstr "Не забывайте отдыхать время от времени"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:68
msgid "Eat fruits if you can 🍐️"
msgstr "Съешьте фруктик, если можете 🍐️"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:69
msgid "Take care of yourself"
msgstr "Займитесь собой"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:70
msgid "Remember to have some fun"
msgstr "Получать удовольствие тоже важно"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:71
msgid "You're doing great"
msgstr "Похоже всё идёт отлично"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:76
msgid "Smile, breathe and go slowly"
msgstr "Улыбнитесь, отдышитесь и не спеша продолжайте"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:77
msgid "Wherever you go, there you are"
msgstr "От себя не убежишь"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:78
msgid "Working hard is always rewarded"
msgstr "Тяжкие труды всегда вознаграждаются"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:79
msgid "Keep calm"
msgstr "Сохраняйте спокойствие"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:80
msgid "You can do it"
msgstr "Вы можете это сделать"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:81
msgid "Meanwhile, spread the love ♥️"
msgstr "А пока дарите любовь ♥️"

#: src/plugins/scheduled-panel/gtd-panel-scheduled.c:345
msgid "Scheduled"
msgstr "Запланировано"

#: src/plugins/task-lists-workspace/gtd-sidebar.c:419
#, c-format
msgid "Task list <b>%s</b> removed"
msgstr "Список задач <b>%s</b> удалён"

#: src/plugins/task-lists-workspace/gtd-sidebar-provider-row.ui:64
msgid "Loading…"
msgstr "Загрузка…"

#. Translators: 'archived' as in 'archived task lists'
#: src/plugins/task-lists-workspace/gtd-sidebar.ui:90
msgid "Archived"
msgstr "Архивировано"

#: src/plugins/task-lists-workspace/gtd-sidebar.ui:127
msgid "No Archived Lists"
msgstr "Нет списков в архиве"

#: src/plugins/task-lists-workspace/gtd-task-list-panel.c:208
msgid "Unarchive"
msgstr "Разархивировать"

#: src/plugins/task-lists-workspace/gtd-task-list-panel.c:208
#: src/plugins/task-lists-workspace/gtd-task-list-panel.ui:52
msgid "Archive"
msgstr "Архивировать"

#: src/plugins/task-lists-workspace/gtd-task-list-panel.c:233
msgid "An error occurred while updating a task"
msgstr "Ошибка при обновлении задачи"

#: src/plugins/task-lists-workspace/gtd-task-list-panel.ui:37
#: src/plugins/task-lists-workspace/gtd-task-list-panel.ui:81
#: src/plugins/task-lists-workspace/gtd-task-list-panel.ui:96
msgid "Rename"
msgstr "Переименовать"

#: src/plugins/task-lists-workspace/gtd-task-list-panel.ui:43
msgid "Clear completed tasks…"
msgstr "Очистить завершённые задачи…"

#: src/plugins/task-lists-workspace/gtd-task-list-panel.ui:58
msgid "Delete"
msgstr "Удалить"

#: src/plugins/task-lists-workspace/gtd-task-lists-workspace.c:407
msgid "Close"
msgstr "Закрыть"

#: src/plugins/task-lists-workspace/gtd-task-lists-workspace.c:444
msgid "Details"
msgstr "Подробности"

#: src/plugins/task-lists-workspace/gtd-task-lists-workspace.c:469
msgid "Task Lists"
msgstr "Списки задач"

#: src/plugins/task-lists-workspace/gtd-task-lists-workspace.ui:32
msgid "New List"
msgstr "Создать список"

#: src/plugins/today-panel/gtd-today-omni-area-addin.c:53
msgid "No more tasks left"
msgstr "Задач больше нет"

#: src/plugins/today-panel/gtd-today-omni-area-addin.c:54
msgid "Nothing else to do here"
msgstr "Здесь больше нечего делать"

#: src/plugins/today-panel/gtd-today-omni-area-addin.c:55
msgid "You made it!"
msgstr "Вы сделали это!"

#: src/plugins/today-panel/gtd-today-omni-area-addin.c:56
msgid "Looks like there’s nothing else left here"
msgstr "Кажется, здесь больше ничего не осталось"

#: src/plugins/today-panel/gtd-today-omni-area-addin.c:94
#, c-format
msgid "%d task for today"
msgid_plural "%d tasks for today"
msgstr[0] "%d задача на сегодня"
msgstr[1] "%d задачи на сегодня"
msgstr[2] "%d задач на сегодня"

#: src/plugins/today-panel/gtd-today-omni-area-addin.c:109
msgid "No tasks scheduled for today"
msgstr "На сегодня не запланировано ни одной задачи"

#~ msgid "Get some rest now"
#~ msgstr "Передохните немного"

#~ msgid "Enjoy the rest of your day"
#~ msgstr "Наслаждайтесь остатком дня"

#~ msgid "Good job!"
#~ msgstr "Хорошая работа!"

#~ msgid "Meanwhile, spread the love"
#~ msgstr "А пока дарите любовь"

#~ msgid "Tasks Will Appear Here"
#~ msgstr "Задачи появятся здесь"

#~ msgid "Add Tasks…"
#~ msgstr "Добавить задачи…"

#~ msgid "Add More Tasks…"
#~ msgstr "Добавить больше задач…"

#~ msgid "Use system style"
#~ msgstr "Используйте системный стиль"

#~ msgid "Light style"
#~ msgstr "Светлый стиль"

#~ msgid "Dark style"
#~ msgstr "Тёмный стиль"

#, c-format
#~ msgid "%1$s and one more task"
#~ msgid_plural "%1$s and %2$d other tasks"
#~ msgstr[0] "%1$s и ещё %2$d задача"
#~ msgstr[1] "%1$s и %2$d других задачи"
#~ msgstr[2] "%1$s и %2$d других задач"

#, c-format
#~ msgid "You have %d task for today"
#~ msgid_plural "You have %d tasks for today"
#~ msgstr[0] "Имеется %d задача на сегодня"
#~ msgstr[1] "Имеется %d задачи на сегодня"
#~ msgstr[2] "Имеется %d задач на сегодня"

#~ msgid "Run Endeavour on startup"
#~ msgstr "Запускать Endeavour при загрузке системы"

#~ msgid "Whether Endeavour should run on startup"
#~ msgstr "Должен ли Endeavour запускаться при загрузке системы"

#~ msgid "Show notifications on startup"
#~ msgstr "Показывать уведомления при загрузке"

#~ msgid "Whether Endeavour should show notifications or not"
#~ msgstr "Должен ли Endeavour показывать уведомления или нет"

#~ msgid "Run on Startup"
#~ msgstr "Запуск при загрузке системы"

#~ msgid "Run Endeavour automatically when you log in"
#~ msgstr "Запускать Endeavour автоматически при входе в систему"

#~ msgid "Show Notifications"
#~ msgstr "Показывать уведомления"

#~ msgid "When Endeavour runs, show a startup notification"
#~ msgstr "При запуске Endeavour показать уведомление о загрузке"

#~ msgid "Endeavour cannot connect to Todoist due to network issue"
#~ msgstr "Endeavour не может подключиться к Todoist из-за проблемы с сетью"

#~ msgid ""
#~ "Not able to communicate with Todoist. Please check your internet "
#~ "connectivity."
#~ msgstr "Отсутствует связь с Todoist. Проверьте интернет соединение."

#~ msgid "Error fetching Todoist account key"
#~ msgstr "Ошибка получения ключа учётной записи Todoist"

#~ msgid "Please ensure that Todoist account is correctly configured."
#~ msgstr "Проверьте правильность настройки учётной записи Todoist."

#, c-format
#~ msgid ""
#~ "Endeavour doesn’t have the necessary permissions to perform this action: "
#~ "%s"
#~ msgstr ""
#~ "Endeavour не имеет необходимых разрешений для выполнения этого действия: "
#~ "%s"

#, c-format
#~ msgid ""
#~ "Invalid response received from Todoist servers. Please reload Endeavour."
#~ msgstr ""
#~ "Получен неверный ответ от серверов Todoist. Пожалуйста, перезагрузите "
#~ "Endeavour."

#~ msgid "An error occurred while updating a Todoist task"
#~ msgstr "Ошибка при обновлении задачи Todoist"

#~ msgid "An error occurred while retrieving Todoist data"
#~ msgstr "Ошибка при получении данных от Todoist"

#~ msgid "An error occurred while updating Todoist"
#~ msgstr "Ошибка при обновлении Todoist"

#~ msgid "Todoist"
#~ msgstr "Todoist"

#, c-format
#~ msgid "Todoist: %s"
#~ msgstr "Todoist: %s"

#~ msgid "No Todoist accounts found"
#~ msgstr "Учётная запись Todoist не найдена"

#~ msgid "Add a Todoist account"
#~ msgstr "Добавить учётную запись Todoist"

#~ msgid "Cannot create Todo.txt file"
#~ msgstr "Не удалось создать файл Todo.txt"

#~ msgid "Select a Todo.txt-formatted file:"
#~ msgstr "Выберите файл в формате Todo.txt:"

#~ msgid "Select a file"
#~ msgstr "Выберите файл"

#~ msgid "Error opening Todo.txt file"
#~ msgstr "Ошибка при открытии файла Todo.txt"

#~ msgid ""
#~ "<b>Warning!</b> Todo.txt support is experimental and unstable. You may "
#~ "experience instability, errors and eventually data loss. It is not "
#~ "recommended to use Todo.txt integration on production systems."
#~ msgstr ""
#~ "<b>Внимание!</b> Поддержка Todo.txt является экспериментальной и "
#~ "нестабильной. Могут встречаться ошибки и вероятна потеря данных. Не "
#~ "рекомендуется использовать Todo.txt на рабочих системах."

#~ msgid "Error while opening the file monitor. Todo.txt will not be monitored"
#~ msgstr ""
#~ "Ошибка при открытии файлового монитора. Todo.txt не будет отслеживаться"

#~ msgid "Todo.txt"
#~ msgstr "Todo.txt"

#~ msgid "On the Todo.txt file"
#~ msgstr "В файле Todo.txt"

#~ msgid "Todo.txt File"
#~ msgstr "Файл Todo.txt"

#~ msgid "Source of the Todo.txt file"
#~ msgstr "Источник файла Todo.txt"

#~ msgid "Unscheduled"
#~ msgstr "Не запланировано"

#, python-format
#~ msgid "Unscheduled (%d)"
#~ msgstr "Не запланировано (%d)"

#, c-format
#~ msgid "Good Morning, %s"
#~ msgstr "Доброе утро, %s"

#, c-format
#~ msgid "Good Afternoon, %s"
#~ msgstr "Добрый день, %s"

#, c-format
#~ msgid "Good Evening, %s"
#~ msgstr "Добрый вечер, %s"

#~ msgid "Home"
#~ msgstr "Домой"

#~ msgid "Task manager for GNOME"
#~ msgstr "Менеджер задач для GNOME"

#~ msgid "Welcome screen"
#~ msgstr "Экран приветствия"

#~ msgid "Style Variant"
#~ msgstr "Вариант стиля"

#~ msgid ""
#~ "Use the light or dark variant of the GTK theme and/or GtkSourceView style "
#~ "scheme."
#~ msgstr ""
#~ "Использовать тёмный или светлый вариант темы GTK и/или стиль темы "
#~ "GtkSourceView."
