# British English translation for gnome-todo.
# Copyright (C) 2016 gnome-todo's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-todo package.
# Bruce Cowan <bruce@bcowan.me.uk>, 2016-2021.
# Zander Brown <zbrown@gnome.org>, 2019-2021.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-todo master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-todo/issues\n"
"POT-Creation-Date: 2021-08-24 15:54+0000\n"
"PO-Revision-Date: 2021-08-28 13:20+0100\n"
"Last-Translator: Zander Brown <zbrown@gnome.org>\n"
"Language-Team: English - United Kingdom <en_GB@li.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 40.0\n"

#: data/appdata/org.gnome.Todo.appdata.xml.in.in:7
#: data/org.gnome.Todo.desktop.in.in:3 src/gui/gtd-application.c:130
#: src/gui/gtd-omni-area.ui:44 src/main.c:38
msgid "To Do"
msgstr "To Do"

#: data/appdata/org.gnome.Todo.appdata.xml.in.in:8
msgid "Task manager for GNOME"
msgstr "Task manager for GNOME"

#: data/appdata/org.gnome.Todo.appdata.xml.in.in:11
msgid ""
"GNOME To Do is a task management application designed to integrate with "
"GNOME. Save and order your todos. Manage multiple todo lists. Keep your "
"todos on your computer or sync them with cloud providers. Customize your "
"experience with plugins."
msgstr ""
"GNOME To Do is a task management application designed to integrate with "
"GNOME. Save and order your todos. Manage multiple todo lists. Keep your "
"todos on your computer or sync them with cloud providers. Customise your "
"experience with plugins."

#: data/appdata/org.gnome.Todo.appdata.xml.in.in:25
#| msgid "Welcome"
msgid "Welcome screen"
msgstr "Welcome screen"

#: data/appdata/org.gnome.Todo.appdata.xml.in.in:29
msgid "Empty state"
msgstr "Empty state"

#: data/appdata/org.gnome.Todo.appdata.xml.in.in:33
#| msgid "Task Lists"
msgid "Task lists"
msgstr "Task lists"

#: data/org.gnome.Todo.desktop.in.in:4
msgid "Manage your personal tasks"
msgstr "Manage your personal tasks"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Todo.desktop.in.in:13
msgid "Task;Productivity;Todo;"
msgstr "Task;Productivity;Todo;"

#: data/org.gnome.todo.gschema.xml:6
msgid "Window maximized"
msgstr "Window maximised"

#: data/org.gnome.todo.gschema.xml:7
msgid "Window maximized state"
msgstr "Window maximised state"

#: data/org.gnome.todo.gschema.xml:11
msgid "Window size"
msgstr "Window size"

#: data/org.gnome.todo.gschema.xml:12
msgid "Window size (width and height)."
msgstr "Window size (width and height)."

#: data/org.gnome.todo.gschema.xml:16
msgid "First run of GNOME To Do"
msgstr "First run of GNOME To Do"

#: data/org.gnome.todo.gschema.xml:17
msgid ""
"Whether it’s the first run of GNOME To Do (to run the initial setup) or not"
msgstr ""
"Whether it’s the first run of GNOME To Do (to run the initial setup) or not"

#: data/org.gnome.todo.gschema.xml:21
msgid "Default provider to add new lists to"
msgstr "Default provider to add new lists to"

#: data/org.gnome.todo.gschema.xml:22
msgid "The identifier of the default provider to add new lists to"
msgstr "The identifier of the default provider to add new lists to"

#: data/org.gnome.todo.gschema.xml:26
msgid "List of active extensions"
msgstr "List of active extensions"

#: data/org.gnome.todo.gschema.xml:27
msgid "The list of active extensions"
msgstr "The list of active extensions"

#: data/org.gnome.todo.gschema.xml:31
msgid "Sidebar revealed"
msgstr "Sidebar revealed"

#: data/org.gnome.todo.gschema.xml:32
msgid "Whether the sidebar is revealed or not"
msgstr "Whether the sidebar is revealed or not"

#: data/org.gnome.todo.gschema.xml:40
msgid "Style Variant"
msgstr "Style Variant"

#: data/org.gnome.todo.gschema.xml:41
msgid ""
"Use the light or dark variant of the GTK theme and/or GtkSourceView style "
"scheme."
msgstr ""
"Use the light or dark variant of the GTK theme and/or GtkSourceView style "
"scheme."

#: src/gui/gtd-application.c:73
msgid "Quit GNOME To Do"
msgstr "Quit GNOME To Do"

#: src/gui/gtd-application.c:74
msgid "Enable debug messages"
msgstr "Enable debug messages"

#: src/gui/gtd-application.c:134
msgid "Copyright © 2015–2020 The To Do authors"
msgstr "Copyright © 2015–2020 The To Do authors"

#: src/gui/gtd-application.c:139
msgid "translator-credits"
msgstr ""
"Bruce Cowan <bruce@bcowan.me.uk>\n"
"Waldo Luís Ribeiro <waldoribeiro@sapo.pt>\n"
"Zander Brown <zbrown@gnome.org>"

#: src/gui/gtd-edit-pane.c:93
#: src/plugins/all-tasks-panel/gtd-all-tasks-panel.c:112
#: src/plugins/next-week-panel/gtd-next-week-panel.c:140
#: src/plugins/scheduled-panel/gtd-panel-scheduled.c:103
msgid "No date set"
msgstr "No date set"

#: src/gui/gtd-edit-pane.ui:16
msgid "D_ue Date"
msgstr "D_ue Date"

#: src/gui/gtd-edit-pane.ui:30
msgid "_Today"
msgstr "_Today"

#: src/gui/gtd-edit-pane.ui:39
msgid "To_morrow"
msgstr "To_morrow"

#: src/gui/gtd-edit-pane.ui:75
msgid "_Notes"
msgstr "_Notes"

#: src/gui/gtd-edit-pane.ui:121
msgid "_Delete"
msgstr "_Delete"

#: src/gui/gtd-edit-pane.ui:158
msgctxt "taskdate"
msgid "None"
msgstr "None"

#: src/gui/gtd-empty-list-widget.c:50
#: src/plugins/today-panel/gtd-today-omni-area-addin.c:53
msgid "No more tasks left"
msgstr "No more tasks left"

#: src/gui/gtd-empty-list-widget.c:51
#: src/plugins/today-panel/gtd-today-omni-area-addin.c:54
msgid "Nothing else to do here"
msgstr "Nothing else to do here"

#: src/gui/gtd-empty-list-widget.c:52
#: src/plugins/today-panel/gtd-today-omni-area-addin.c:55
msgid "You made it!"
msgstr "You made it!"

#: src/gui/gtd-empty-list-widget.c:53
#: src/plugins/today-panel/gtd-today-omni-area-addin.c:56
msgid "Looks like there’s nothing else left here"
msgstr "Looks like there’s nothing else left here"

#: src/gui/gtd-empty-list-widget.c:58
msgid "Get some rest now"
msgstr "Get some rest now"

#: src/gui/gtd-empty-list-widget.c:59
msgid "Enjoy the rest of your day"
msgstr "Enjoy the rest of your day"

#: src/gui/gtd-empty-list-widget.c:60
msgid "Good job!"
msgstr "Good job!"

#: src/gui/gtd-empty-list-widget.c:61
msgid "Meanwhile, spread the love"
msgstr "Meanwhile, spread the love"

#: src/gui/gtd-empty-list-widget.c:62
#: src/plugins/peace/gtd-peace-omni-area-addin.c:78
msgid "Working hard is always rewarded"
msgstr "Working hard is always rewarded"

#: src/gui/gtd-empty-list-widget.c:72
msgid "Tasks Will Appear Here"
msgstr "Tasks Will Appear Here"

#: src/gui/gtd-empty-list-widget.c:74
msgid "Add Tasks…"
msgstr "Add Tasks…"

#: src/gui/gtd-empty-list-widget.c:85
msgid "Add More Tasks…"
msgstr "Add More Tasks…"

#: src/gui/gtd-initial-setup-window.ui:20
msgid "Welcome"
msgstr "Welcome"

#: src/gui/gtd-initial-setup-window.ui:31
msgid "Log in to online accounts to access your tasks"
msgstr "Log in to online accounts to access your tasks"

#: src/gui/gtd-initial-setup-window.ui:53
msgid "To Do Setup"
msgstr "To Do Setup"

#: src/gui/gtd-initial-setup-window.ui:56 src/gui/gtd-provider-popover.ui:42
msgid "_Cancel"
msgstr "_Cancel"

#: src/gui/gtd-initial-setup-window.ui:65
msgid "_Done"
msgstr "_Done"

#. Translators: %1$s is the task list name, %2$s is the provider name
#: src/gui/gtd-new-task-row.c:90
#, c-format
msgid "%1$s \t <small>%2$s</small>"
msgstr "%1$s \t <small>%2$s</small>"

#: src/gui/gtd-new-task-row.c:127
msgid "An error occurred while creating a task"
msgstr "An error occurred while creating a task"

#: src/gui/gtd-new-task-row.ui:13
msgid "New task…"
msgstr "New task…"

#: src/gui/gtd-provider-popover.c:97
msgid "An error occurred while creating a task list"
msgstr "An error occurred while creating a task list"

#: src/gui/gtd-provider-popover.ui:25
msgid "Create _List"
msgstr "Create _List"

#: src/gui/gtd-provider-popover.ui:55
msgid "List Name"
msgstr "List Name"

#: src/gui/gtd-provider-popover.ui:115
msgid "Select a storage location"
msgstr "Select a storage location"

#: src/gui/gtd-provider-row.ui:60
msgid "Off"
msgstr "Off"

#: src/gui/gtd-provider-selector.ui:19
msgid "Click to add a new Google account"
msgstr "Click to add a new Google account"

#: src/gui/gtd-provider-selector.ui:35
msgid "Google"
msgstr "Google"

#: src/gui/gtd-provider-selector.ui:45
msgid "Click to add a new ownCloud account"
msgstr "Click to add a new ownCloud account"

#: src/gui/gtd-provider-selector.ui:61
msgid "ownCloud"
msgstr "ownCloud"

#: src/gui/gtd-provider-selector.ui:71
msgid "Click to add a new Microsoft Exchange account"
msgstr "Click to add a new Microsoft Exchange account"

#: src/gui/gtd-provider-selector.ui:87
msgid "Microsoft Exchange"
msgstr "Microsoft Exchange"

#: src/gui/gtd-provider-selector.ui:100
msgid "Or you can just store your tasks on this computer"
msgstr "Or you can just store your tasks on this computer"

#: src/gui/gtd-task-list-view.c:463
#, c-format
msgid "Task <b>%s</b> removed"
msgstr "Task <b>%s</b> removed"

#: src/gui/gtd-task-list-view.c:482
#: src/plugins/task-lists-workspace/gtd-sidebar.c:422
msgid "Undo"
msgstr "Undo"

#: src/gui/gtd-task-row.c:127
#: src/plugins/all-tasks-panel/gtd-all-tasks-panel.c:133
#: src/plugins/next-week-panel/gtd-next-week-panel.c:152
#: src/plugins/scheduled-panel/gtd-panel-scheduled.c:124
#: src/plugins/today-panel/gtd-panel-today.c:177
#: src/plugins/today-panel/gtd-panel-today.c:295
#: src/plugins/welcome/gtd-welcome-workspace.ui:56
msgid "Today"
msgstr "Today"

#: src/gui/gtd-task-row.c:131
#: src/plugins/all-tasks-panel/gtd-all-tasks-panel.c:137
#: src/plugins/next-week-panel/gtd-next-week-panel.c:156
#: src/plugins/scheduled-panel/gtd-panel-scheduled.c:128
msgid "Tomorrow"
msgstr "Tomorrow"

#: src/gui/gtd-task-row.c:135
#: src/plugins/all-tasks-panel/gtd-all-tasks-panel.c:129
#: src/plugins/scheduled-panel/gtd-panel-scheduled.c:120
msgid "Yesterday"
msgstr "Yesterday"

#: src/gui/gtd-theme-selector.ui:21
msgid "Light style"
msgstr "Light style"

#: src/gui/gtd-theme-selector.ui:34
msgid "Dark style"
msgstr "Dark style"

#: src/gui/gtd-window.c:111
msgid ""
"This is a development build of To Do. You may experience errors, wrong "
"behaviors, and data loss."
msgstr ""
"This is a development build of To Do. You may experience errors, wrong "
"behaviours, and data loss."

#: src/gui/gtd-window.c:340
msgid "Details"
msgstr "Details"

#: src/gui/menus.ui:13
msgid "_Help"
msgstr "_Help"

#: src/gui/menus.ui:17
msgid "_About To Do"
msgstr "_About To Do"

#. Translators: This message will never be used with '1 day ago'
#. * but the singular form is required because some languages do not
#. * have plurals, some languages reuse the singular form for numbers
#. * like 21, 31, 41, etc.
#.
#: src/plugins/all-tasks-panel/gtd-all-tasks-panel.c:125
#: src/plugins/scheduled-panel/gtd-panel-scheduled.c:116
#, c-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "%d day ago"
msgstr[1] "%d days ago"

#: src/plugins/all-tasks-panel/gtd-all-tasks-panel.c:335
msgid "All"
msgstr "All"

#: src/plugins/background/gtd-plugin-background.c:238
#, c-format
msgid "%1$s and one more task"
msgid_plural "%1$s and %2$d other tasks"
msgstr[0] "%1$s and one more task"
msgstr[1] "%1$s and %2$d other tasks"

#: src/plugins/background/gtd-plugin-background.c:342
#, c-format
msgid "You have %d task for today"
msgid_plural "You have %d tasks for today"
msgstr[0] "You have %d task for today"
msgstr[1] "You have %d tasks for today"

#: src/plugins/background/org.gnome.todo.background.gschema.xml:6
msgid "Run To Do on startup"
msgstr "Run To Do on startup"

#: src/plugins/background/org.gnome.todo.background.gschema.xml:7
msgid "Whether GNOME To Do should run on startup"
msgstr "Whether GNOME To Do should run on startup"

#: src/plugins/background/org.gnome.todo.background.gschema.xml:11
msgid "Show notifications on startup"
msgstr "Show notifications on startup"

#: src/plugins/background/org.gnome.todo.background.gschema.xml:12
msgid "Whether GNOME To Do should show notifications or not"
msgstr "Whether GNOME To Do should show notifications or not"

#: src/plugins/background/ui/preferences.ui:41
msgid "Run on Startup"
msgstr "Run on Startup"

#: src/plugins/background/ui/preferences.ui:52
msgid "Run To Do automatically when you log in"
msgstr "Run To Do automatically when you log in"

#: src/plugins/background/ui/preferences.ui:101
msgid "Show Notifications"
msgstr "Show Notifications"

#: src/plugins/background/ui/preferences.ui:112
msgid "When To Do runs, show a startup notification"
msgstr "When To Do runs, show a startup notification"

#: src/plugins/eds/gtd-plugin-eds.c:181
msgid "Error loading GNOME Online Accounts"
msgstr "Error loading GNOME Online Accounts"

#: src/plugins/eds/gtd-provider-eds.c:190
msgid "Failed to connect to task list"
msgstr "Failed to connect to task list"

#: src/plugins/eds/gtd-provider-local.c:50
msgid "On This Computer"
msgstr "On This Computer"

#: src/plugins/eds/gtd-provider-local.c:62
msgid "Local"
msgstr "Local"

#: src/plugins/eds/gtd-task-list-eds.c:407
#: src/plugins/eds/gtd-task-list-eds.c:434
#: src/plugins/eds/gtd-task-list-eds.c:458
msgid "Error fetching tasks from list"
msgstr "Error fetching tasks from list"

#: src/plugins/eds/gtd-task-list-eds.c:828
#: src/plugins/inbox-panel/gtd-inbox-panel.c:111
#: src/plugins/welcome/gtd-welcome-workspace.ui:47
msgid "Inbox"
msgstr "Inbox"

#: src/plugins/next-week-panel/gtd-next-week-panel.c:148
#: src/plugins/today-panel/gtd-panel-today.c:168
msgid "Overdue"
msgstr "Overdue"

#: src/plugins/next-week-panel/gtd-next-week-panel.c:388
msgid "Next 7 Days"
msgstr "Next 7 Days"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:56
msgid "Did you drink some water today?"
msgstr "Did you drink some water today?"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:57
msgid "What are your goals for today?"
msgstr "What are your goals for today?"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:58
msgid "Can you let your creativity flow?"
msgstr "Can you let your creativity flow?"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:59
msgid "How are you feeling right now?"
msgstr "How are you feeling right now?"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:60
msgid "At what point is it good enough?"
msgstr "At what point is it good enough?"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:65
msgid "Remember to breathe. Good. Don't stop."
msgstr "Remember to breathe. Good. Don't stop."

#: src/plugins/peace/gtd-peace-omni-area-addin.c:66
msgid "Don't forget to drink some water"
msgstr "Don't forget to drink some water"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:67
msgid "Remember to take some time off"
msgstr "Remember to take some time off"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:68
msgid "Eat fruits if you can 🍐️"
msgstr "Eat fruits if you can 🍐️"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:69
msgid "Take care of yourself"
msgstr "Take care of yourself"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:70
msgid "Remember to have some fun"
msgstr "Remember to have some fun"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:71
msgid "You're doing great"
msgstr "You're doing well"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:76
msgid "Smile, breathe and go slowly"
msgstr "Smile, breathe and go slowly"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:77
msgid "Wherever you go, there you are"
msgstr "Wherever you go, there you are"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:79
msgid "Keep calm"
msgstr "Keep calm"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:80
msgid "You can do it"
msgstr "You can do it"

#: src/plugins/peace/gtd-peace-omni-area-addin.c:81
msgid "Meanwhile, spread the love ♥️"
msgstr "Meanwhile, spread the love ♥️"

#: src/plugins/scheduled-panel/gtd-panel-scheduled.c:345
msgid "Scheduled"
msgstr "Scheduled"

#: src/plugins/task-lists-workspace/gtd-sidebar.c:419
#, c-format
msgid "Task list <b>%s</b> removed"
msgstr "Task list <b>%s</b> removed"

#: src/plugins/task-lists-workspace/gtd-sidebar-provider-row.ui:69
msgid "Loading…"
msgstr "Loading…"

#. Translators: 'archived' as in 'archived task lists'
#: src/plugins/task-lists-workspace/gtd-sidebar.ui:94
msgid "Archived"
msgstr "Archived"

#: src/plugins/task-lists-workspace/gtd-sidebar.ui:146
msgid "No archived lists"
msgstr "No archived lists"

#: src/plugins/task-lists-workspace/gtd-task-list-panel.c:208
msgid "Unarchive"
msgstr "Unarchive"

#: src/plugins/task-lists-workspace/gtd-task-list-panel.c:208
#: src/plugins/task-lists-workspace/gtd-task-list-panel.ui:48
msgid "Archive"
msgstr "Archive"

#: src/plugins/task-lists-workspace/gtd-task-list-panel.c:233
msgid "An error occurred while updating a task"
msgstr "An error occurred while updating a task"

#: src/plugins/task-lists-workspace/gtd-task-list-panel.ui:33
#: src/plugins/task-lists-workspace/gtd-task-list-panel.ui:77
#: src/plugins/task-lists-workspace/gtd-task-list-panel.ui:92
msgid "Rename"
msgstr "Rename"

#: src/plugins/task-lists-workspace/gtd-task-list-panel.ui:39
#: src/plugins/unscheduled-panel/unscheduled-panel/__init__.py:62
msgid "Clear completed tasks…"
msgstr "Clear completed tasks…"

#: src/plugins/task-lists-workspace/gtd-task-list-panel.ui:54
msgid "Delete"
msgstr "Delete"

#: src/plugins/task-lists-workspace/gtd-task-lists-workspace.c:303
msgid "Task Lists"
msgstr "Task Lists"

#: src/plugins/task-lists-workspace/gtd-task-lists-workspace.ui:48
msgid "New List"
msgstr "New List"

#: src/plugins/today-panel/gtd-today-omni-area-addin.c:94
#, c-format
msgid "%d task for today"
msgid_plural "%d tasks for today"
msgstr[0] "%d task for today"
msgstr[1] "%d tasks for today"

#: src/plugins/today-panel/gtd-today-omni-area-addin.c:109
msgid "No tasks scheduled for today"
msgstr "No tasks scheduled for today"

#: src/plugins/todoist/gtd-plugin-todoist.c:109
msgid "GNOME To Do cannot connect to Todoist due to network issue"
msgstr "GNOME To Do cannot connect to Todoist due to network issue"

#: src/plugins/todoist/gtd-plugin-todoist.c:110
msgid ""
"Not able to communicate with Todoist. Please check your internet "
"connectivity."
msgstr ""
"Not able to communicate with Todoist. Please check your internet "
"connectivity."

#: src/plugins/todoist/gtd-provider-todoist.c:213
msgid "Error fetching Todoist account key"
msgstr "Error fetching Todoist account key"

#: src/plugins/todoist/gtd-provider-todoist.c:214
msgid "Please ensure that Todoist account is correctly configured."
msgstr "Please ensure that Todoist account is correctly configured."

#: src/plugins/todoist/gtd-provider-todoist.c:555
#, c-format
msgid ""
"GNOME To Do doesn’t have the necessary permissions to perform this action: %s"
msgstr ""
"GNOME To Do doesn’t have the necessary permissions to perform this action: %s"

#: src/plugins/todoist/gtd-provider-todoist.c:563
#, c-format
msgid ""
"Invalid response received from Todoist servers. Please reload GNOME To Do."
msgstr ""
"Invalid response received from Todoist servers. Please reload GNOME To Do."

#: src/plugins/todoist/gtd-provider-todoist.c:850
msgid "An error occurred while updating a Todoist task"
msgstr "An error occurred while updating a Todoist task"

#: src/plugins/todoist/gtd-provider-todoist.c:965
msgid "An error occurred while retrieving Todoist data"
msgstr "An error occurred while retrieving Todoist data"

#: src/plugins/todoist/gtd-provider-todoist.c:1040
msgid "An error occurred while updating Todoist"
msgstr "An error occurred while updating Todoist"

#: src/plugins/todoist/gtd-provider-todoist.c:1100
msgid "Todoist"
msgstr "Todoist"

#: src/plugins/todoist/gtd-provider-todoist.c:1411
#, c-format
msgid "Todoist: %s"
msgstr "Todoist: %s"

#: src/plugins/todoist/ui/preferences.ui:65
msgid "No Todoist accounts found"
msgstr "No Todoist accounts found"

#: src/plugins/todoist/ui/preferences.ui:74
msgid "Add a Todoist account"
msgstr "Add a Todoist account"

#: src/plugins/todo-txt/gtd-plugin-todo-txt.c:87
#: src/plugins/todo-txt/gtd-plugin-todo-txt.c:124
msgid "Cannot create Todo.txt file"
msgstr "Cannot create Todo.txt file"

#: src/plugins/todo-txt/gtd-plugin-todo-txt.c:161
msgid "Select a Todo.txt-formatted file:"
msgstr "Select a Todo.txt-formatted file:"

#. Filechooser
#: src/plugins/todo-txt/gtd-plugin-todo-txt.c:165
msgid "Select a file"
msgstr "Select a file"

#: src/plugins/todo-txt/gtd-plugin-todo-txt.c:191
msgid "Error opening Todo.txt file"
msgstr "Error opening Todo.txt file"

#: src/plugins/todo-txt/gtd-plugin-todo-txt.c:201
msgid ""
"<b>Warning!</b> Todo.txt support is experimental and unstable. You may "
"experience instability, errors and eventually data loss. It is not "
"recommended to use Todo.txt integration on production systems."
msgstr ""
"<b>Warning!</b> Todo.txt support is experimental and unstable. You may "
"experience instability, errors and eventually data loss. It is not "
"recommended to use Todo.txt integration on production systems."

#: src/plugins/todo-txt/gtd-provider-todo-txt.c:574
msgid "Error while opening the file monitor. Todo.txt will not be monitored"
msgstr "Error while opening the file monitor. Todo.txt will not be monitored"

#: src/plugins/todo-txt/gtd-provider-todo-txt.c:643
msgid "Todo.txt"
msgstr "Todo.txt"

#: src/plugins/todo-txt/gtd-provider-todo-txt.c:655
msgid "On the Todo.txt file"
msgstr "On the Todo.txt file"

#: src/plugins/todo-txt/org.gnome.todo.txt.gschema.xml:6
msgid "Todo.txt File"
msgstr "Todo.txt File"

#: src/plugins/todo-txt/org.gnome.todo.txt.gschema.xml:7
msgid "Source of the Todo.txt file"
msgstr "Source of the Todo.txt file"

#. Translators: 'Unscheduled' as in 'Unscheduled tasks'
#: src/plugins/unscheduled-panel/unscheduled-panel/__init__.py:41
#: src/plugins/unscheduled-panel/unscheduled-panel/__init__.py:112
msgid "Unscheduled"
msgstr "Unscheduled"

#. Translators: 'Unscheduled' as in 'Unscheduled tasks'
#: src/plugins/unscheduled-panel/unscheduled-panel/__init__.py:115
#, python-format
msgid "Unscheduled (%d)"
msgstr "Unscheduled (%d)"

#: src/plugins/welcome/gtd-welcome-workspace.c:68
#, c-format
msgid "Good Morning, %s"
msgstr "Good Morning, %s"

#: src/plugins/welcome/gtd-welcome-workspace.c:70
#, c-format
msgid "Good Afternoon, %s"
msgstr "Good Afternoon, %s"

#: src/plugins/welcome/gtd-welcome-workspace.c:72
#, c-format
msgid "Good Evening, %s"
msgstr "Good Evening, %s"

#: src/plugins/welcome/gtd-welcome-workspace.c:206
msgid "Home"
msgstr "Home"

#~ msgid "GNOME To Do with the dark theme variant"
#~ msgstr "GNOME To Do with the dark theme variant"

#~ msgid "Editing a tasklist with GNOME To Do"
#~ msgstr "Editing a tasklist with GNOME To Do"

#~ msgid "Task lists displayed on grid mode"
#~ msgstr "Task lists displayed on grid mode"

#~ msgid "Task lists displayed on list mode"
#~ msgstr "Task lists displayed on list mode"

#~ msgid "Available plugins for GNOME To Do"
#~ msgstr "Available plugins for GNOME To Do"

#~ msgid "Visualizing tasks for today on GNOME To Do’s Today panel"
#~ msgstr "Visualising tasks for today on GNOME To Do’s Today panel"

#~ msgid "No tasks found"
#~ msgstr "No tasks found"

#~ msgid "You can add tasks using the <b>+</b> above"
#~ msgstr "You can add tasks using the <b>+</b> above"

#~ msgid "Error loading extension"
#~ msgstr "Error loading extension"

#~ msgid "Error unloading extension"
#~ msgstr "Error unloading extension"

#~ msgid "Extensions"
#~ msgstr "Extensions"

#~ msgid "No extensions found"
#~ msgstr "No extensions found"

#~ msgid "The current list selector"
#~ msgstr "The current list selector"

#~ msgid "The current list selector. Can be “grid” or “list”."
#~ msgstr "The current list selector. Can be “grid” or “list”."

#~ msgid "An error occurred while modifying a task"
#~ msgstr "An error occurred while modifying a task"

#~ msgid "An error occurred while removing a task"
#~ msgstr "An error occurred while removing a task"

#~ msgid "An error occurred while modifying a task list"
#~ msgstr "An error occurred while modifying a task list"

#~ msgid "Click a task list to select"
#~ msgstr "Click a task list to select"

#~ msgid "_Quit"
#~ msgstr "_Quit"

#~ msgid "org.gnome.Todo"
#~ msgstr "org.gnome.Todo"

#~ msgid "Window position"
#~ msgstr "Window position"

#~ msgid "Window position (x and y)."
#~ msgstr "Window position (x and y)."

#~ msgid "_Priority"
#~ msgstr "_Priority"

#~ msgctxt "taskpriority"
#~ msgid "None"
#~ msgstr "None"

#~ msgid "Low"
#~ msgstr "Low"

#~ msgid "Medium"
#~ msgstr "Medium"

#~ msgid "High"
#~ msgstr "High"

#~ msgid "Copyright © %1$d The To Do authors"
#~ msgstr "Copyright © %1$d The To Do authors"

#~ msgid "Removing this task will also remove its subtasks. Remove anyway?"
#~ msgstr "Removing this task will also remove its subtasks. Remove anyway?"

#~ msgid "Once removed, the tasks cannot be recovered."
#~ msgstr "Once removed, the tasks cannot be recovered."

#~ msgid "Cancel"
#~ msgstr "Cancel"

#~ msgid "Remove"
#~ msgstr "Remove"

#~| msgid "_Rename"
#~ msgid "Rename %s"
#~ msgstr "Rename %s"

#~ msgid "Tasks"
#~ msgstr "Tasks"

#~ msgid "Name of the task list"
#~ msgstr "Name of the task list"

#~ msgid "Show or hide completed tasks"
#~ msgstr "Show or hide completed tasks"

#~ msgid "Done"
#~ msgstr "Done"

#~| msgid "Done"
#~ msgid "Done (%d)"
#~ msgstr "Done (%d)"

#~ msgid "Loading your task lists…"
#~ msgstr "Loading your task lists…"

#~ msgid "No tasks"
#~ msgstr "No tasks"

#~ msgid "Remove the selected task lists?"
#~ msgstr "Remove the selected task lists?"

#~ msgid "Once removed, the task lists cannot be recovered."
#~ msgstr "Once removed, the task lists cannot be recovered."

#~ msgid "Remove task lists"
#~ msgstr "Remove task lists"

#~| msgid "Clear completed tasks…"
#~ msgid "Clear completed tasks"
#~ msgstr "Clear completed tasks"

#~ msgid "New List…"
#~ msgstr "New List…"

#~ msgid "Next _Week"
#~ msgstr "Next _Week"

#~ msgid "Notes"
#~ msgstr "Notes"

#~ msgid "Due Date"
#~ msgstr "Due Date"

#~ msgid "Priority"
#~ msgstr "Priority"

#~ msgid "Error loading Evolution-Data-Server backend"
#~ msgstr "Error loading Evolution-Data-Server backend"

#~ msgid "Task list source successfully connected"
#~ msgstr "Task list source successfully connected"

#~ msgid "Failed to prompt for credentials"
#~ msgstr "Failed to prompt for credentials"

#~ msgid "Failed to prompt for credentials for"
#~ msgstr "Failed to prompt for credentials for"

#~ msgid "Authentication failure"
#~ msgstr "Authentication failure"

#~ msgid "Error loading task manager"
#~ msgstr "Error loading task manager"

#~ msgid "Error updating task"
#~ msgstr "Error updating task"

#~ msgid "Error saving task list"
#~ msgstr "Error saving task list"

#~ msgid "Error creating new task list"
#~ msgstr "Error creating new task list"

#~ msgid "Bad status code (%d) received. Please check your connection."
#~ msgstr "Bad status code (%d) received. Please check your connection."

#~ msgid "Error while reading a line from Todo.txt"
#~ msgstr "Error while reading a line from Todo.txt"

#~ msgid "Incorrect date"
#~ msgstr "Incorrect date"

#~ msgid "Please make sure the date in Todo.txt is valid."
#~ msgstr "Please make sure the date in Todo.txt is valid."

#~ msgid "Unrecognized token in a Todo.txt line"
#~ msgstr "Unrecognised token in a Todo.txt line"

#~ msgid ""
#~ "To Do cannot recognize some tags in your Todo.txt file. Some tasks may "
#~ "not be loaded"
#~ msgstr ""
#~ "To Do cannot recognise some tags in your Todo.txt file. Some tasks may "
#~ "not be loaded"

#~ msgid "No task list found for some tasks"
#~ msgstr "No task list found for some tasks"

#~ msgid ""
#~ "Some of the tasks in your Todo.txt file do not have a task list. To Do "
#~ "supports tasks with a task list. Please add a list to all your tasks"
#~ msgstr ""
#~ "Some of the tasks in your Todo.txt file do not have a task list. To Do "
#~ "supports tasks with a task list. Please add a list to all your tasks"

#~ msgid "Setting new color for task list"
#~ msgstr "Setting new colour for task list"

#~ msgid "Change default storage location…"
#~ msgstr "Change default storage location…"

#~ msgid "Select the default storage location to create task lists:"
#~ msgstr "Select the default storage location to create task lists:"

#~ msgid "Default storage location"
#~ msgstr "Default storage location"

#~ msgid "Error loading CSS from resource"
#~ msgstr "Error loading CSS from resource"

#~ msgid "unscheduled-panel"
#~ msgstr "unscheduled-panel"
