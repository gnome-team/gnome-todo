# Ukrainian translation for gnome-todo.
# Copyright (C) 2021 gnome-todo's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-todo package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: gnome-todo master\n"
"POT-Creation-Date: 2022-08-10 20:44+0000\n"
"PO-Revision-Date: 2022-08-12 23:41+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "Юрій Чорноіван <yurchor@ukr.net>, 2021"

#. (itstool) path: info/desc
#: C/index.page:7
msgid ""
"Endeavour is a simple app, with that you can create, process and manage your "
"tasks, to raise your productivity. Also you can synchronize your tasks "
"through your online accounts, so you can check them on all your devices."
msgstr ""
"«Endeavour» — проста програма, за допомогою якої ви можете створювати записи "
"завдань, обробляти записи завдань та керувати записами завдань для "
"підвищення продуктивності. Також ви можете синхронізувати ваші завдання із "
"вашими обліковими записами в інтернеті, щоб з ними можна було працювати на "
"усіх ваших пристроях."

#. (itstool) path: page/title
#: C/index.page:10
msgid "<_:media-1/> <span>Endeavour</span>"
msgstr "<_:media-1/> <span>Endeavour</span>"

#. (itstool) path: page/p
#: C/index.page:15
msgid "A simple to-do app for GNOME."
msgstr "Проста програма для керування завданнями для GNOME."

#. (itstool) path: section/title
#: C/index.page:18
msgid "Getting Started"
msgstr "Початкові зауваження"

#. (itstool) path: section/title
#: C/index.page:22
msgid "Manage Tasks"
msgstr "Керування завданнями"

#. (itstool) path: section/title
#: C/index.page:26
msgid "Manage Task Lists"
msgstr "Керування списками завдань"

#. (itstool) path: section/title
#: C/index.page:30
msgid "Common Problems"
msgstr "Типові проблеми"

#. (itstool) path: section/title
#: C/index.page:34
msgid "Contributing and getting support"
msgstr "Участь у розробці та отримання підтримки"

#. (itstool) path: page/title
#: C/add-notes.page:9
msgid "Add notes to a task"
msgstr "Додавання нотаток до завдання"

#. (itstool) path: item/p
#: C/add-notes.page:12 C/assign-date.page:12
msgid "Expand the task by clicking on it in the list."
msgstr "Розгорніть запис завдання клацанням на ньому у списку."

#. (itstool) path: item/p
#: C/add-notes.page:13
msgid ""
"Write something in the textfield labeled with <gui style=\"button\">Notes</"
"gui>."
msgstr ""
"Впишіть щось у текстове поле із міткою <gui style=\"button\">Нотатки</gui>."

#. (itstool) path: page/p
#: C/add-notes.page:16
msgid "You can edit this later whenever you want."
msgstr "Ви зможете змінити ваш запис пізніше, якщо захочете."

#. (itstool) path: page/title
#: C/archive.page:10
msgid "Archive a task list"
msgstr "Архівування списку завдань"

#. (itstool) path: item/p
#: C/archive.page:14 C/assign-color.page:13 C/delete-tasklist.page:19
#: C/rename-tasklist.page:13
msgid "Navigate to your task list."
msgstr "Перейдіть до вашого списку завдань."

#. (itstool) path: item/p
#: C/archive.page:15 C/archive.page:23
msgid "Open the menu on the right side of the headerbar."
msgstr "Відкрийте меню у правій частині смужки заголовка."

#. (itstool) path: item/p
#: C/archive.page:16
msgid ""
"Click on the <gui style=\"button\">Archive</gui> button to archive your task "
"list."
msgstr ""
"Натисніть кнопку <gui style=\"button\">Архівувати</gui>, щоб архівувати ваш "
"список завдань."

#. (itstool) path: page/p
#: C/archive.page:19
msgid ""
"You can access your archived task lists through the <gui>Archived</gui> "
"category in the utility pane."
msgstr ""
"Отримати доступ до списку архівованих завдань можна за допомогою категорії "
"<gui>Архівовано</gui> на панелі допоміжних засобів."

#. (itstool) path: page/p
#: C/archive.page:20
msgid "From there you can unarchive them:"
msgstr "З цієї панелі ви можете скасувати архівування списків завдань:"

#. (itstool) path: item/p
#: C/archive.page:24
msgid ""
"The <gui style=\"button\">Unarchive</gui> button moves your task list again "
"to all others."
msgstr ""
"Натискання кнопки <gui style=\"button\">Скасувати архівування</gui> пересуне "
"ваш архівований список завдань до усіх інших."

#. (itstool) path: page/title
#: C/assign-color.page:10
msgid "Assign a color to a task list"
msgstr "Призначення кольору до списку завдань"

#. (itstool) path: item/p
#: C/assign-color.page:14 C/delete-tasklist.page:20 C/rename-tasklist.page:14
msgid "Open the menu (<_:media-1/>) on the right side of the headerbar."
msgstr "Відкрийте меню ( <_:media-1/> ) у правій частині смужки заголовка."

#. (itstool) path: item/p
#: C/assign-color.page:15
msgid "Choose one of the given colors by clicking on them."
msgstr ""
"Виберіть один із визначених кольорів натисканням на відповідному пункті."

#. (itstool) path: page/title
#: C/assign-date.page:9
msgid "Assign a date to a task"
msgstr "Призначення дати до завдання"

#. (itstool) path: item/p
#: C/assign-date.page:13
msgid ""
"Click on the button labeled with <gui style=\"button\">Today</gui>, <gui "
"style=\"button\">Tomorrow</gui> or with the arrow icon to assign another "
"date."
msgstr ""
"Натисніть кнопку із міткою <gui style=\"button\">Сьогодні</gui>, <gui style="
"\"button\">Завтра</gui> або піктограмою стрілки, щоб призначити іншу дату."

#. (itstool) path: page/p
#: C/assign-date.page:16
msgid ""
"To delete the date, click on the button with the arrow icon and then on <gui "
"style=\"button\">None</gui>."
msgstr ""
"Щоб вилучити дату, натисніть кнопку із піктограмою стрілки, а потім виберіть "
"пункт <gui style=\"button\">Немає</gui>."

#. (itstool) path: page/title
#: C/contributing.page:9
msgid "Contribute to Endeavour"
msgstr "Участь у розробці «Endeavour»"

#. (itstool) path: page/p
#: C/contributing.page:11
msgid "There are multiple ways of helping to improve Endeavour:"
msgstr "Існує декілька способів допомогти в удосконаленні «Endeavour»:"

#. (itstool) path: item/p
#: C/contributing.page:13
msgid "File issues you have encountered while using Endeavour"
msgstr ""
"Надіслати звіти щодо вад, з якими ви зіткнулися під час користування "
"«Endeavour»"

#. (itstool) path: item/p
#: C/contributing.page:14
msgid "Translate the user interface or help pages of Endeavour"
msgstr "Перекласти інтерфейс користувача або сторінки довідки «Endeavour»"

#. (itstool) path: item/p
#: C/contributing.page:15
msgid "Correct issues or implement new features"
msgstr "Виправити вади або реалізувати нові можливості"

#. (itstool) path: page/p
#: C/contributing.page:18
msgid ""
"For filing an issue go to the <link href=\"https://gitlab.gnome.org/World/"
"Endeavour/issues\">issuetracker</link>."
msgstr ""
"Для створення звітів щодо вад скористайтеся <link href=\"https://gitlab."
"gnome.org/World/Endeavour/issues\">системою стеження за вадами</link>."

#. (itstool) path: page/p
#: C/contributing.page:19
msgid ""
"And for contributing code or translations go to the <link href=\"https://"
"wiki.gnome.org/action/show/Newcomers\">newcomers guide</link>."
msgstr ""
"Для ознайомлення зі способами надсилання коду та перекладів скористайтеся "
"<link href=\"https://wiki.gnome.org/action/show/Newcomers\">підручником для "
"початківців</link>."

#. (itstool) path: page/title
#: C/create-tasklists.page:9
msgid "Create a task list"
msgstr "Створення списку завдань"

#. (itstool) path: item/p
#: C/create-tasklists.page:12
msgid ""
"Click on the <gui style=\"button\">New List</gui> button in the headerbar."
msgstr ""
"Натисніть кнопку <gui style=\"button\">Новий список</gui> на смужці "
"заголовка."

#. (itstool) path: item/p
#: C/create-tasklists.page:13
msgid "Type in the name of your task list."
msgstr "Введіть назву вашого списку завдань."

#. (itstool) path: item/p
#: C/create-tasklists.page:14
msgid ""
"Then hit <keyseq><key>Enter</key></keyseq> or click on <gui style=\"button"
"\">Create List</gui>."
msgstr ""
"Потім натисніть клавішу <keyseq><key>Enter</key></keyseq> або кнопку <gui "
"style=\"button\">Створити список</gui>."

#. (itstool) path: page/p
#: C/create-tasklists.page:17
msgid "Now you can access your list through the entries in the utility pane."
msgstr ""
"Тепер ви можете отримувати доступ до вашого списку за допомогою записів на "
"панелі допоміжних засобів."

#. (itstool) path: page/title
#: C/create-tasks.page:9
msgid "Create a Task"
msgstr "Створити завдання"

#. (itstool) path: item/p
#: C/create-tasks.page:12
msgid ""
"Type the title of the new task in the textfield labeled with <gui style="
"\"button\">New Task</gui> next to + ."
msgstr ""
"Введіть заголовок нового завдання у текстове поле з міткою <gui style="
"\"button\">Нове завдання</gui> поряд із + ."

#. (itstool) path: item/p
#: C/create-tasks.page:13
msgid "Then hit <keyseq><key>Enter</key></keyseq> or click on + to save it."
msgstr ""
"Натисніть клавішу <keyseq><key>Enter</key></keyseq> або кнопку +, щоб "
"зберегти його."

#. (itstool) path: note/title
#: C/create-tasks.page:17 C/delete-tasklist.page:13
msgid "Undoing changes"
msgstr "Скасовування змін"

#. (itstool) path: note/p
#: C/create-tasks.page:18
msgid ""
"Note that you can always undo deleting a task if you change your mind. Click "
"<gui style=\"button\">Undo</gui> on the pop-up notification that appears. "
"But when the popup disappears, you cannot recover it anymore."
msgstr ""
"Зауважте, що ви завжди можете скасувати вилучення завдання, якщо "
"передумаєте. Натисніть кнопку <gui style=\"button\">Скасувати</gui> на "
"показаній панелі контекстного сповіщення. Втім, коли контекстна панель "
"зникне, ви вже не зможете відновити запис завдання."

#. (itstool) path: page/p
#: C/create-tasks.page:22
msgid ""
"When you want to delete the task, expand the task by clicking on it in the "
"list and then click on <gui style=\"button\">Delete</gui>."
msgstr ""
"Якщо ви хочете вилучити завдання, розгорніть його пункт клацанням на ньому у "
"списку, а потім натисніть кнопку <gui style=\"button\">Вилучити</gui>."

#. (itstool) path: page/title
#: C/delete-tasklist.page:10
msgid "Delete a task list"
msgstr "Вилучення списку завдань"

#. (itstool) path: note/p
#: C/delete-tasklist.page:14
msgid ""
"Note that you can always undo deleting a task list if you change your mind. "
"Click <gui style=\"button\">Undo</gui> on the pop-up notification that "
"appears. But when the popup disappears, you cannot recover it anymore."
msgstr ""
"Зауважте, що ви завжди можете скасувати вилучення списку завдань, якщо "
"передумаєте. Натисніть кнопку <gui style=\"button\">Скасувати</gui> на "
"показаній панелі контекстного сповіщення. Втім, коли контекстна панель "
"зникне, ви вже не зможете відновити список завдань."

#. (itstool) path: item/p
#: C/delete-tasklist.page:21
msgid ""
"Then the <gui style=\"button\">Delete</gui> button deletes your task list."
msgstr ""
"Потім натискання кнопки <gui style=\"button\">Вилучити</gui> призведе до "
"вилучення вашого списку завдань."

#. (itstool) path: page/title
#: C/execute-task.page:9
msgid "Execute a task"
msgstr "Виконання завдання"

#. (itstool) path: page/p
#: C/execute-task.page:11
msgid ""
"Click on the empty field on the left side of the task in the list, to give "
"it a check. Then it disappears from <gui>Inbox</gui>, and similar "
"categories. When the task was in a task list, you can still find it there "
"and remove the check to make it again not executed."
msgstr ""
"Клацніть на порожньому полі ліворуч від запису завдання у списку, щоб "
"позначити його. Запис завдання зникне з теки <gui>Вхідні</gui> і подібних "
"категорій. коли завдання потрапить до списку завдань, ви можете знайти його "
"пункт і вилучити позначку, щоб знову перевести його у категорію невиконаних."

#. (itstool) path: page/title
#: C/give-star.page:9
msgid "Give a star to a task"
msgstr "Надання рейтингу завданню"

#. (itstool) path: page/p
#: C/give-star.page:11
msgid ""
"Sometimes you have tasks with a very high priority or want to find them "
"easier in a long list. In that case you can give tasks a star."
msgstr ""
"Іноді у вас можуть бути завдання дуже високої пріоритетності або вам "
"потрібно буде спростити пошук завдання у довгому списку. У цьому випадку "
"можна позначити завдання зірочкою."

#. (itstool) path: page/p
#: C/give-star.page:13
msgid ""
"Click on the small star on the right side of the task in the list to toggle "
"its state. When it is filled you have starred it."
msgstr ""
"Натисніть невеличку зірочку праворуч від пункту завдання у списку, щоб "
"перемкнути стан запису завдання. Якщо зірочку заповнено, завдання позначено."

#. (itstool) path: page/title
#: C/overview.page:9
msgid "Overview"
msgstr "Огляд"

#. (itstool) path: page/p
#: C/overview.page:13
msgid ""
"The user interface of Endeavour is composed of the utility pane on the left "
"and the tasks in lists on the right. You can let you show your tasks in "
"different modes. They can be selected through the utility pane."
msgstr ""
"Інтерфейс користувача Endeavour складається з панелі допоміжних засобів "
"ліворуч та списків завдань праворуч. Завдання можна переглядати у різних "
"режимах. Вибрати режим можна за допомогою панелі допоміжних засобів."

#. (itstool) path: page/p
#: C/overview.page:15
msgid ""
"<gui>Inbox</gui> contains all tasks which have no date assigned to them and "
"are not in a task list,"
msgstr ""
"<gui>Вхідні</gui> містить усі завдання, з якими не пов'язано дат і яких "
"немає у списку завдань,"

#. (itstool) path: page/p
#: C/overview.page:17
msgid ""
"<gui>Today</gui> all tasks with the date of today and <gui>Next 7 Days</gui> "
"all tasks assigned to one of the next seven days."
msgstr ""
"<gui>Сьогодні</gui> — усі завдання, датою яких є поточна, і <gui>Наступні 7 "
"днів</gui> — усі завдання, які призначено на один з наступних семи днів."

#. (itstool) path: page/p
#: C/overview.page:19
#| msgid ""
#| "<gui>All</gui> shows all tasks in a chronological order and "
#| "<gui>Archived</gui> archived task lists. At least there are also your "
#| "custom task lists"
msgid ""
"<gui>All</gui> shows all tasks in a chronological order and <gui>Archived</"
"gui> archived task lists. At last there are also your custom task lists"
msgstr ""
"<gui>Усі</gui> — показує усі завдання у хронологічному порядку, а "
"<gui>Архівовані</gui> — архівовані списки завдань. Нарешті, передбачено "
"ваші нетипові списки завдань."

#. (itstool) path: page/title
#: C/rename-tasks.page:9
msgid "Rename a Task"
msgstr "Перейменування завдання"

#. (itstool) path: item/p
#: C/rename-tasks.page:12
msgid "Go to the title of the task in the list, the cursor should change then."
msgstr "Перейдіть до заголовка завдання у списку, і курсор має змінитися."

#. (itstool) path: item/p
#: C/rename-tasks.page:13
msgid "Click on it, and then begin renaming it."
msgstr "Клацніть на заголовку і змініть назву."

#. (itstool) path: item/p
#: C/rename-tasks.page:14
msgid "When you are ready, hit <keyseq><key>Enter</key></keyseq>."
msgstr ""
"Щойно потрібний текст буде введено, натисніть клавішу <keyseq><key>Enter</"
"key></keyseq>."

#. (itstool) path: page/title
#: C/rename-tasklist.page:10
msgid "Rename a task list"
msgstr "Перейменування списку завдань"

#. (itstool) path: item/p
#: C/rename-tasklist.page:15
msgid "Click on the <gui style=\"button\">Rename</gui> button."
msgstr "Натисніть кнопку <gui style=\"button\">Перейменувати</gui>."

#. (itstool) path: item/p
#: C/rename-tasklist.page:16
msgid "Type in the new name."
msgstr "Введіть нову назву."

#. (itstool) path: item/p
#: C/rename-tasklist.page:17
msgid ""
"Hit <keyseq><key>Enter</key></keyseq> or click <gui style=\"button\">Rename</"
"gui>."
msgstr ""
"Натисніть клавішу <keyseq><key>Enter</key></keyseq> або кнопку <gui style="
"\"button\">Перейменувати</gui>."

#. (itstool) path: page/title
#: C/support.page:9
msgid "Getting support"
msgstr "Отримання підтримки"

#. (itstool) path: page/p
#: C/support.page:11
msgid ""
"In the <link href=\"https://wiki.gnome.org/apps/todo\">wiki</link> of "
"Endeavour you can read more."
msgstr ""
"Докладніший опис можна знайти у <link href=\"https://wiki.gnome.org/apps/todo"
"\">вікі</link> «Endeavour»."

#. (itstool) path: page/p
#: C/support.page:12
msgid ""
"When you have questions, ask them on <link href=\"https://discourse.gnome."
"org/tag/todo\">gnome discourse</link>."
msgstr ""
"Якщо у вас виникли питання, поставте їх у <link href=\"https://discourse."
"gnome.org/tag/todo\">gnome discourse</link>."

#. (itstool) path: page/p
#: C/support.page:13
msgid ""
"You can also get in conversation with the developers of Endeavour by joining "
"the <link href=\"https://gnome.element.io/#/room/#gnome-todo:gnome.org"
"\">matrix channel</link>."
msgstr ""
"Ви також можете поспілкуватися із розробниками «Endeavour», долучившись до "
"обговорення на <link href=\"https://gnome.element.io/#/room/#gnome-todo:"
"gnome.org\">каналі у matrix</link>."

#. (itstool) path: page/title
#: C/switch-theme.page:10
msgid "Switch to the dark theme"
msgstr "Перемикання на темну тему"

#. (itstool) path: page/p
#: C/switch-theme.page:12
msgid ""
"When you are in a dark environment, for example at night, a dark theme can "
"be much more comfortable for the eyes."
msgstr ""
"Коли ви перебуваєте у темному середовищі, наприклад вночі, темна тема є "
"набагато комфортнішою для очей."

#. (itstool) path: item/p
#: C/switch-theme.page:15
msgid "Open the menu ( <_:media-1/> ) on the right side of the headerbar."
msgstr "Відкрийте меню ( <_:media-1/> ) у правій частині смужки заголовка."

#. (itstool) path: item/p
#: C/switch-theme.page:16
msgid "Click on the dark or light circle, and the theme switches to it."
msgstr ""
"Натисніть на темному або світлому колі, і тему буде перемкнено на "
"відповідний варіант."

#. (itstool) path: page/title
#: C/workflow.page:9
msgid "Workflow"
msgstr "Процес"

#. (itstool) path: page/p
#: C/workflow.page:11
msgid ""
"Managing tasks is very personal. Therefore Endeavour only ships with a "
"default Workflow that can be altered to the needs of the user."
msgstr ""
"Керування завданнями є доволі особистою справою. Тому «Endeavour» "
"постачаються із типовим робочим процесом, який може бути змінено відповідно "
"до уподобань користувача."

#. (itstool) path: section/title
#: C/workflow.page:14
msgid "Task lifecycle"
msgstr "Життєвий цикл завдання"

#. (itstool) path: section/p
#: C/workflow.page:15
msgid "Tasks follow a lifecycle that consists of 3 phases:"
msgstr "Життєвий цикл завдань складається з 3 фаз:"

#. (itstool) path: item/p
#: C/workflow.page:18
msgid "Capture"
msgstr "Захоплення"

#. (itstool) path: item/p
#: C/workflow.page:19
msgid "Processing"
msgstr "Оброблення"

#. (itstool) path: item/p
#: C/workflow.page:20
msgid "Execution"
msgstr "Виконання"

#. (itstool) path: section/p
#: C/workflow.page:23
msgid ""
"Each phase changes the state of the task, starting from the uncaptured "
"state, leading up to the task completion."
msgstr ""
"На кожній з фаз стан завдання змінюється, починаючи зі стану до захоплення, "
"аж до стану завершеності завдання."

#. (itstool) path: section/title
#: C/workflow.page:27
msgid "1. Capturing"
msgstr "1. Захоплення"

#. (itstool) path: section/p
#: C/workflow.page:28
msgid ""
"Capturing a task is the act of storing the task in a permanent storage - "
"your hard drive."
msgstr ""
"Захоплення завдання — дія зі збереження запису завдання у сховищі — на диску "
"вашого комп'ютера."

#. (itstool) path: section/p
#: C/workflow.page:29
msgid "Captured tasks go to the inbox, which is the list of unprocessed tasks."
msgstr ""
"Захоплені завдання потрапляють до вхідної теки, тобто списку необроблених "
"завдань."

#. (itstool) path: section/title
#: C/workflow.page:33
msgid "2. Processing"
msgstr "2. Оброблення"

#. (itstool) path: section/p
#: C/workflow.page:34
msgid ""
"Processing a task consists of moving it to an appropriate task list, or "
"doing it right away if it's a trivial task, or even throwing it in the "
"wastebasket if you don't plan on doing it."
msgstr ""
"Оброблення завдання полягає у пересуванні його запису до відповідного списку "
"завдань, або виконанні його, якщо це тривіальне завдання, або навіть "
"викиданні запису завдання у смітник, якщо ви не плануєте його виконувати."

#. (itstool) path: section/p
#: C/workflow.page:35
msgid ""
"Optionally, adding more description, setting an end date, and marking it as "
"important are done in this step."
msgstr ""
"Крім того, на цьому кроці можна додати опис завдання, встановити дату "
"завершення або позначити завдання як важливе."

#. (itstool) path: section/title
#: C/workflow.page:39
msgid "3. Executing"
msgstr "3. Виконання"

#. (itstool) path: section/p
#: C/workflow.page:40
msgid "Executing a task is what leads the task to its conclusion."
msgstr "Виконання завдання — те, що переводить завдання у стан завершеності."

#~ msgid ""
#~ "The user interface of Endeavour is composed of the utility pane on the "
#~ "left and the tasks in lists on the right. You can let you show your tasks "
#~ "in different modes. They can be selected through the utility pane. "
#~ "<gui>Inbox</gui> contains all tasks which have no date assigned to them "
#~ "and are not in a task list, <gui>Today</gui> all tasks with the date of "
#~ "today and <gui>Next 7 Days</gui> all tasks assigned to one of the next "
#~ "seven days. <gui>All</gui> shows all tasks in a chronological order and "
#~ "<gui>Archived</gui> archived task lists. At least there are also your "
#~ "custom task lists"
#~ msgstr ""
#~ "Інтерфейс користувача «Endeavour» складається із панелі допоміжних "
#~ "засобів ліворуч і списку завдань праворуч. Переглядати завдання можна у "
#~ "різних режимах. Вибрати режим можна за допомогою панелі допоміжних "
#~ "засобів. У списку <gui>Вхідні</gui> містяться записи усіх завдань, з "
#~ "якими не пов'язано дати і списку завдань, у списку <gui>Сьогодні</gui> ви "
#~ "знайдете записи усіх завдань, які призначено на сьогодні, а у списку "
#~ "<gui>Наступні 7 днів</gui> — усі завдання, які призначено на будь-який з "
#~ "наступних семи днів. У списку <gui>Усі</gui> буде наведено записи усіх "
#~ "завдань у хронологічному порядку, а у списку <gui>Архівовані</gui> — усі "
#~ "архівовані списки завдань. Крім того, можуть бути ваші нетипові списки "
#~ "завдань."

#~ msgctxt "_"
#~ msgid ""
#~ "external ref='figures/org.gnome.Todo.svg' "
#~ "md5='9e1744fd1202132056fcdd4b6d012067'"
#~ msgstr ""
#~ "external ref='figures/org.gnome.Todo.svg' "
#~ "md5='9e1744fd1202132056fcdd4b6d012067'"

#~ msgid ""
#~ "<media type=\"image\" its:translatable=\"no\" src=\"figures/org.gnome."
#~ "Todo.svg\" width=\"48\" height=\"48\"/> <span>GNOME To Do</span>"
#~ msgstr ""
#~ "<media type=\"image\" its:translatable=\"no\" src=\"figures/org.gnome."
#~ "Todo.svg\" width=\"48\" height=\"48\"/> <span>Завдання GNOME</span>"
